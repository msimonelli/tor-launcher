<!ENTITY torsettings.dialog.title "Gosodiadau Rhwydwaith Tor">
<!ENTITY torsettings.wizard.title.default "Connect to Tor">
<!ENTITY torsettings.wizard.title.configure "Gosodiadau Rhwydwaith Tor">
<!ENTITY torsettings.wizard.title.connecting "Establishing a Connection">

<!-- For locale picker: -->
<!ENTITY torlauncher.localePicker.title "Iaith Porwr Tor">
<!ENTITY torlauncher.localePicker.prompt "Dewiswch iaith">

<!-- For "first run" wizard: -->

<!ENTITY torSettings.connectPrompt "Click “Connect” to connect to Tor.">
<!ENTITY torSettings.configurePrompt "Click “Configure” to adjust network settings if you are in a country that censors Tor (such as Egypt, China, Turkey) or if you are connecting from a private network that requires a proxy.">
<!ENTITY torSettings.configure "Ffurfweddu">
<!ENTITY torSettings.connect "Cysylltu">

<!-- Other: -->

<!ENTITY torsettings.startingTor "Yn aros am Tor i gychwyn…">
<!ENTITY torsettings.restartTor "Ailgychwyn Tor">
<!ENTITY torsettings.reconfigTor "Reconfigure">

<!ENTITY torsettings.discardSettings.prompt "You have configured Tor bridges or you have entered local proxy settings.&#160; To make a direct connection to the Tor network, these settings must be removed.">
<!ENTITY torsettings.discardSettings.proceed "Remove Settings and Connect">

<!ENTITY torsettings.optional "Dewisiol">

<!ENTITY torsettings.useProxy.checkbox "I use a proxy to connect to the Internet">
<!ENTITY torsettings.useProxy.type "Proxy Type">
<!ENTITY torsettings.useProxy.type.placeholder "select a proxy type">
<!ENTITY torsettings.useProxy.address "Address">
<!ENTITY torsettings.useProxy.address.placeholder "Cyfeiriad IP neu enw gwesteiwr">
<!ENTITY torsettings.useProxy.port "Port">
<!ENTITY torsettings.useProxy.username "Enw defnyddiwr">
<!ENTITY torsettings.useProxy.password "Cyfrinair">
<!ENTITY torsettings.useProxy.type.socks4 "SOCKS 4">
<!ENTITY torsettings.useProxy.type.socks5 "SOCKS 5">
<!ENTITY torsettings.useProxy.type.http "HTTP / HTTPS">
<!ENTITY torsettings.firewall.checkbox "This computer goes through a firewall that only allows connections to certain ports">
<!ENTITY torsettings.firewall.allowedPorts "Allowed Ports">
<!ENTITY torsettings.useBridges.checkbox "Tor is censored in my country">
<!ENTITY torsettings.useBridges.default "Select a built-in bridge">
<!ENTITY torsettings.useBridges.default.placeholder "select a bridge">
<!ENTITY torsettings.useBridges.bridgeDB "Request a bridge from torproject.org">
<!ENTITY torsettings.useBridges.captchaSolution.placeholder "Enter the characters from the image">
<!ENTITY torsettings.useBridges.reloadCaptcha.tooltip "Get a new challenge">
<!ENTITY torsettings.useBridges.captchaSubmit "Cyflwyno">
<!ENTITY torsettings.useBridges.custom "Provide a bridge I know">
<!ENTITY torsettings.useBridges.label "Enter bridge information from a trusted source.">
<!ENTITY torsettings.useBridges.placeholder "type address:port (one per line)">

<!ENTITY torsettings.copyLog "Copio Cofnod Tor i'r Clipfwrdd">

<!ENTITY torsettings.proxyHelpTitle "Proxy Help">
<!ENTITY torsettings.proxyHelp1 "A local proxy might be needed when connecting through a company, school, or university network.&#160;If you are not sure whether a proxy is needed, look at the Internet settings in another browser or check your system's network settings.">

<!ENTITY torsettings.bridgeHelpTitle "Cymorth Pont Relái">
<!ENTITY torsettings.bridgeHelp1 "Bridges are unlisted relays that make it more difficult to block connections to the Tor Network.&#160; Each type of bridge uses a different method to avoid censorship.&#160; The obfs ones make your traffic look like random noise, and the meek ones make your traffic look like it's connecting to that service instead of Tor.">
<!ENTITY torsettings.bridgeHelp2 "Because of how certain countries try to block Tor, certain bridges work in certain countries but not others.&#160; If you are unsure about which bridges work in your country, visit torproject.org/about/contact.html#support">

<!-- Progress -->
<!ENTITY torprogress.pleaseWait "Please wait while we establish a connection to the Tor network.&#160; This may take several minutes.">

<!-- #31286 about:preferences strings -->
<!ENTITY torPreferences.categoryTitle "Connection">
<!ENTITY torPreferences.torSettings "Tor Settings">
<!ENTITY torPreferences.torSettingsDescription "Tor Browser routes your traffic over the Tor Network, run by thousands of volunteers around the world." >
<!ENTITY torPreferences.learnMore "Learn more">
<!-- Status -->
<!ENTITY torPreferences.statusInternetLabel "Internet:">
<!ENTITY torPreferences.statusInternetTest "Test">
<!ENTITY torPreferences.statusInternetOnline "Online">
<!ENTITY torPreferences.statusInternetOffline "Offline">
<!ENTITY torPreferences.statusTorLabel "Tor Network:">
<!ENTITY torPreferences.statusTorConnected "Connected">
<!ENTITY torPreferences.statusTorNotConnected "Not Connected">
<!ENTITY torPreferences.statusTorBlocked "Potentially Blocked">
<!ENTITY torPreferences.learnMore "Learn more">
<!-- Quickstart -->
<!ENTITY torPreferences.quickstart "Quickstart">
<!ENTITY torPreferences.quickstartDescriptionLong "Quickstart connects Tor Browser to the Tor Network automatically when launched, based on your last used connection settings.">
<!ENTITY torPreferences.quickstartCheckbox "Always connect automatically">
<!-- Bridge settings -->
<!ENTITY torPreferences.bridges "Bridges">
<!ENTITY torPreferences.bridgesDescription "Bridges help you access the Tor Network in places where Tor is blocked. Depending on where you are, one bridge may work better than another.">
<!ENTITY torPreferences.bridgeLocation "Your location">
<!ENTITY torPreferences.bridgeLocationAutomatic "Automatic">
<!ENTITY torPreferences.bridgeLocationFrequent "Frequently selected locations">
<!ENTITY torPreferences.bridgeLocationOther "Other locations">
<!ENTITY torPreferences.bridgeChooseForMe "Choose a Bridge For Me…">
<!ENTITY torPreferences.bridgeBadgeCurrent "Your Current Bridges">
<!ENTITY torPreferences.bridgeBadgeCurrentDescription "You can keep one or more bridges saved, and Tor will choose which one to use when you connect. Tor will automatically switch to use another bridge when needed.">
<!ENTITY torPreferences.bridgeId "#1 bridge: #2"> <!-- #1 = bridge type; #2 = bridge emoji id -->
<!ENTITY torPreferences.remove "Remove">
<!ENTITY torPreferences.bridgeDisableBuiltIn "Disable built-in bridges">
<!ENTITY torPreferences.bridgeShare "Share this bridge using the QR code or by copying its address:">
<!ENTITY torPreferences.bridgeCopy "Copy Bridge Address">
<!ENTITY torPreferences.copied "Copied!">
<!ENTITY torPreferences.bridgeShowAll "Show All Bridges">
<!ENTITY torPreferences.bridgeRemoveAll "Remove All Bridges">
<!ENTITY torPreferences.bridgeAdd "Add a New Bridge">
<!ENTITY torPreferences.bridgeSelectBrowserBuiltin "Choose from one of Tor Browser’s built-in bridges">
<!ENTITY torPreferences.bridgeSelectBuiltin "Select a Built-In Bridge…">
<!ENTITY torPreferences.bridgeRequest "Request a Bridge…">
<!ENTITY torPreferences.bridgeEnterKnown "Enter a bridge address you already know">
<!ENTITY torPreferences.bridgeAddManually "Add a Bridge Manually…">
<!-- Advanced settings -->
<!ENTITY torPreferences.advanced "Advanced">
<!ENTITY torPreferences.advancedDescription "Configure how Tor Browser connects to the internet">
<!ENTITY torPreferences.advancedButton "Settings…">
<!ENTITY torPreferences.viewTorLogs "View the Tor logs">
<!ENTITY torPreferences.viewLogs "View Logs…">
<!-- Remove all bridges dialog -->
<!ENTITY torPreferences.removeBridgesQuestion "Remove all the bridges?">
<!ENTITY torPreferences.removeBridgesWarning "This action cannot be undone.">
<!ENTITY torPreferences.cancel "Diddymu">
<!-- Scan bridge QR dialog -->
<!ENTITY torPreferences.scanQrTitle "Scan the QR code">
<!-- Builtin bridges dialog -->
<!ENTITY torPreferences.builtinBridgeTitle "Built-In Bridges">
<!ENTITY torPreferences.builtinBridgeDescription "Tor Browser includes some specific types of bridges known as “pluggable transports”.">
<!ENTITY torPreferences.builtinBridgeObfs4 "obfs4">
<!ENTITY torPreferences.builtinBridgeObfs4Description "obfs4 is a type of built-in bridge that makes your Tor traffic look random. They are also less likely to be blocked than their predecessors, obfs3 bridges.">
<!ENTITY torPreferences.builtinBridgeSnowflake "Snowflake">
<!ENTITY torPreferences.builtinBridgeSnowflakeDescription "Snowflake is a built-in bridge that defeats censorship by routing your connection through Snowflake proxies, ran by volunteers.">
<!ENTITY torPreferences.builtinBridgeMeekAzure "meek-azure">
<!ENTITY torPreferences.builtinBridgeMeekAzureDescription "meek-azure is a built-in bridge that makes it look like you are using a Microsoft web site instead of using Tor.">
<!-- Request bridges dialog -->
<!ENTITY torPreferences.requestBridgeDialogTitle "Request Bridge">
<!ENTITY torPreferences.requestBridgeDialogWaitPrompt "Contacting BridgeDB. Please Wait.">
<!ENTITY torPreferences.requestBridgeDialogSolvePrompt "Solve the CAPTCHA to request a bridge.">
<!ENTITY torPreferences.requestBridgeErrorBadSolution "The solution is not correct. Please try again.">
<!-- Provide bridge dialog -->
<!ENTITY torPreferences.provideBridgeTitle "Provide Bridge">
<!ENTITY torPreferences.provideBridgeHeader "Enter bridge information from a trusted source">
<!-- Connection settings dialog -->
<!ENTITY torPreferences.connectionSettingsDialogTitle "Connection Settings">
<!ENTITY torPreferences.connectionSettingsDialogHeader "Configure how Tor Browser connects to the Internet">
<!ENTITY torPreferences.firewallPortsPlaceholder "Comma-seperated values">
<!-- Log dialog -->
<!ENTITY torPreferences.torLogsDialogTitle "Tor Logs">

<!-- #24746 about:torconnect strings -->
<!ENTITY torConnect.notConnectedConcise "Not Connected">
<!ENTITY torConnect.connectingConcise "Connecting…">
<!ENTITY torConnect.tryingAgain "Trying again…">
<!ENTITY torConnect.noInternet "Tor Browser couldn’t reach the Internet">
<!ENTITY torConnect.couldNotConnect "Tor Browser could not connect to Tor">
<!ENTITY torConnect.assistDescriptionConfigure "configure your connection"> <!-- used as a text to insert as a link on several strings (#1) -->
<!ENTITY torConnect.assistDescription "If Tor is blocked in your location, trying a bridge may help. Connection assist can choose one for you using your location, or you can #1 manually instead."> <!-- #1 = "configure your connection" link -->
<!ENTITY torConnect.tryingBridge "Trying a bridge…">
<!ENTITY torConnect.tryingBridgeAgain "Trying one more time…">
<!ENTITY torConnect.errorLocation "Tor Browser couldn’t locate you">
<!ENTITY torConnect.errorLocationDescription "Tor Browser needs to know your location in order to choose the right bridge for you. If you’d rather not share your location, #1 manually instead."> <!-- #1 = "configure your connection" link -->
<!ENTITY torConnect.isLocationCorrect "Are these location settings correct?">
<!ENTITY torConnect.isLocationCorrectDescription "Tor Browser still couldn’t connect to Tor. Please check your location settings are correct and try again, or #1 instead."> <!-- #1 = "configure your connection" link -->
<!ENTITY torConnect.breadcrumbAssist "Connection assist">
<!ENTITY torConnect.breadcrumbLocation "Location settings">
<!ENTITY torConnect.breadcrumbTryBridge "Try a bridge">
<!ENTITY torConnect.automatic "Automatic">
<!ENTITY torConnect.selectCountryRegion "Select Country or Region">
<!ENTITY torConnect.frequentLocations "Frequently selected locations">
<!ENTITY torConnect.otherLocations "Other locations">
<!ENTITY torConnect.restartTorBrowser "Restart Tor Browser">
<!ENTITY torConnect.configureConnection "Configure Connection…">
<!ENTITY torConnect.viewLog "View logs…">
<!ENTITY torConnect.tryAgain "Try Again">
<!ENTITY torConnect.offline "Internet not reachable">
<!ENTITY torConnect.connectMessage "Changes to Tor Settings will not take effect until you connect">
<!ENTITY torConnect.tryAgainMessage "Tor Browser has failed to establish a connection to the Tor Network">
<!ENTITY torConnect.yourLocation "Your Location">
<!ENTITY torConnect.tryBridge "Try a Bridge">
<!ENTITY torConnect.autoBootstrappingFailed "Automatic configuration failed">
<!ENTITY torConnect.autoBootstrappingFailed "Automatic configuration failed">
<!ENTITY torConnect.cannotDetermineCountry "Unable to determine user country">
<!ENTITY torConnect.noSettingsForCountry "No settings available for your location">
