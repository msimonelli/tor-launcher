<!ENTITY torsettings.dialog.title "Tor ネットワーク設定">
<!ENTITY torsettings.wizard.title.default "Tor に接続する">
<!ENTITY torsettings.wizard.title.configure "Tor ネットワーク設定">
<!ENTITY torsettings.wizard.title.connecting "接続を確立しています">

<!-- For locale picker: -->
<!ENTITY torlauncher.localePicker.title "Tor Browser 言語">
<!ENTITY torlauncher.localePicker.prompt "言語を選択してください。">

<!-- For "first run" wizard: -->

<!ENTITY torSettings.connectPrompt "“接続” をクリックすると Tor に接続します。">
<!ENTITY torSettings.configurePrompt "Tor による通信を検閲する国（エジプト、中国、トルコ等）にいる場合やプロキシを要求するプライベートネットワークから接続する場合、「設定」をクリックしてネットワーク設定を調整します。">
<!ENTITY torSettings.configure "設定">
<!ENTITY torSettings.connect "接続">

<!-- Other: -->

<!ENTITY torsettings.startingTor "Torの開始を待っています...">
<!ENTITY torsettings.restartTor "Tor を再起動">
<!ENTITY torsettings.reconfigTor "再設定">

<!ENTITY torsettings.discardSettings.prompt "Tor Bridge が設定されるか、ローカルプロキシ設定が入力されるかしました。&#160; Tor ネットワークに直接接続するためには、これらの設定は削除する必要があります。">
<!ENTITY torsettings.discardSettings.proceed "設定と接続を削除">

<!ENTITY torsettings.optional "オプション">

<!ENTITY torsettings.useProxy.checkbox "インターネット接続にプロキシを使用する">
<!ENTITY torsettings.useProxy.type "プロキシの種類">
<!ENTITY torsettings.useProxy.type.placeholder "プロキシの種類を選択">
<!ENTITY torsettings.useProxy.address "アドレス">
<!ENTITY torsettings.useProxy.address.placeholder "IP アドレスまたはホスト名">
<!ENTITY torsettings.useProxy.port "ポート">
<!ENTITY torsettings.useProxy.username "ユーザー名">
<!ENTITY torsettings.useProxy.password "パスワード">
<!ENTITY torsettings.useProxy.type.socks4 "SOCKS 4">
<!ENTITY torsettings.useProxy.type.socks5 "SOCKS 5">
<!ENTITY torsettings.useProxy.type.http "HTTP / HTTPS">
<!ENTITY torsettings.firewall.checkbox "このコンピューターは特定のポートへの接続のみ許可するファイアーウォールを通します。">
<!ENTITY torsettings.firewall.allowedPorts "許可されたポート">
<!ENTITY torsettings.useBridges.checkbox "Tor は私の国では検閲されています">
<!ENTITY torsettings.useBridges.default "内蔵ブリッジを選択">
<!ENTITY torsettings.useBridges.default.placeholder "ブリッジを選択">
<!ENTITY torsettings.useBridges.bridgeDB "torproject.org に Bridge をリクエストする">
<!ENTITY torsettings.useBridges.captchaSolution.placeholder "画像の文字を入力してください">
<!ENTITY torsettings.useBridges.reloadCaptcha.tooltip "チャレンジを更新する">
<!ENTITY torsettings.useBridges.captchaSubmit "送信">
<!ENTITY torsettings.useBridges.custom "既知のブリッジを使用">
<!ENTITY torsettings.useBridges.label "信頼できる情報源から入手したブリッジ情報を入力してください。">
<!ENTITY torsettings.useBridges.placeholder "“アドレス:ポート” の形式で入力 (1 行ずつ)">

<!ENTITY torsettings.copyLog "Tor のログをクリップボードにコピー">

<!ENTITY torsettings.proxyHelpTitle "プロキシヘルプ">
<!ENTITY torsettings.proxyHelp1 "会社、学校、大学等のネットワークを通して接続する場合、ローカルプロキシが必要になる場合があります。&#160;プロキシが必要であるかどうかわからない場合は、他のブラウザのインターネット設定を見るか、システムのネットワーク設定を確認してください。">

<!ENTITY torsettings.bridgeHelpTitle "ブリッジリレーのヘルプ">
<!ENTITY torsettings.bridgeHelp1 "ブリッジとは Tor ネットワークへの接続を検閲することを困難するための非公開のリレーです。&#160; 各種類のブリッジは検閲を避けるための異なる手法を利用しています。&#160; obfs はあなたのトラフィックをランダムなノイズのように見せかけ，meek はあなたのトラフィックを Tor ではない特定のサービスへの接続であるように見せかけます。">
<!ENTITY torsettings.bridgeHelp2 "特定の国が Tor を検閲するしようと試みている場合、それがどのような手法で行われているかによって、ある Bridge がある国では機能しても他の国では動かないという場合があります。&#160; もしどのブリッジがあなたの国で機能するかわからない場合は  torproject.org/about/contact.html#support にアクセスしてください。">

<!-- Progress -->
<!ENTITY torprogress.pleaseWait "Tor ネットワークへの接続が確立されるまでお待ちください。&#160; これには数分間かかることがあります。">

<!-- #31286 about:preferences strings -->
<!ENTITY torPreferences.categoryTitle "接続">
<!ENTITY torPreferences.torSettings "Tor の設定">
<!ENTITY torPreferences.torSettingsDescription "Tor Browser は通信トラフィックを Tor ネットワークを経由させて送信します。Tor ネットワークは世界中の何千ものボランティアによって運用されています。" >
<!ENTITY torPreferences.learnMore "詳細情報">
<!-- Status -->
<!ENTITY torPreferences.statusInternetLabel "Internet:">
<!ENTITY torPreferences.statusInternetTest "テスト">
<!ENTITY torPreferences.statusInternetOnline "オンライン">
<!ENTITY torPreferences.statusInternetOffline "オフライン">
<!ENTITY torPreferences.statusTorLabel "Tor Network:">
<!ENTITY torPreferences.statusTorConnected "接続済み">
<!ENTITY torPreferences.statusTorNotConnected "接続していません">
<!ENTITY torPreferences.statusTorBlocked "Potentially Blocked">
<!ENTITY torPreferences.learnMore "詳細情報">
<!-- Quickstart -->
<!ENTITY torPreferences.quickstart "クイックスタート">
<!ENTITY torPreferences.quickstartDescriptionLong "Quickstart connects Tor Browser to the Tor Network automatically when launched, based on your last used connection settings.">
<!ENTITY torPreferences.quickstartCheckbox "常に自動的に接続">
<!-- Bridge settings -->
<!ENTITY torPreferences.bridges "Bridge">
<!ENTITY torPreferences.bridgesDescription "Bridge は Tor がブロックされる地域から Tor ネットワークにアクセスすることを手助けします。地域によって、ある Bridge が他のものよりうまく動作する可能性があります。">
<!ENTITY torPreferences.bridgeLocation "Your location">
<!ENTITY torPreferences.bridgeLocationAutomatic "自動">
<!ENTITY torPreferences.bridgeLocationFrequent "Frequently selected locations">
<!ENTITY torPreferences.bridgeLocationOther "Other locations">
<!ENTITY torPreferences.bridgeChooseForMe "Choose a Bridge For Me…">
<!ENTITY torPreferences.bridgeBadgeCurrent "Your Current Bridges">
<!ENTITY torPreferences.bridgeBadgeCurrentDescription "You can keep one or more bridges saved, and Tor will choose which one to use when you connect. Tor will automatically switch to use another bridge when needed.">
<!ENTITY torPreferences.bridgeId "#1 bridge: #2"> <!-- #1 = bridge type; #2 = bridge emoji id -->
<!ENTITY torPreferences.remove "削除">
<!ENTITY torPreferences.bridgeDisableBuiltIn "Disable built-in bridges">
<!ENTITY torPreferences.bridgeShare "Share this bridge using the QR code or by copying its address:">
<!ENTITY torPreferences.bridgeCopy "Copy Bridge Address">
<!ENTITY torPreferences.copied "コピーされました">
<!ENTITY torPreferences.bridgeShowAll "Show All Bridges">
<!ENTITY torPreferences.bridgeRemoveAll "Remove All Bridges">
<!ENTITY torPreferences.bridgeAdd "Add a New Bridge">
<!ENTITY torPreferences.bridgeSelectBrowserBuiltin "Choose from one of Tor Browser’s built-in bridges">
<!ENTITY torPreferences.bridgeSelectBuiltin "Select a Built-In Bridge…">
<!ENTITY torPreferences.bridgeRequest "ブリッジをリクエスト。
">
<!ENTITY torPreferences.bridgeEnterKnown "Enter a bridge address you already know">
<!ENTITY torPreferences.bridgeAddManually "Add a Bridge Manually…">
<!-- Advanced settings -->
<!ENTITY torPreferences.advanced "詳細">
<!ENTITY torPreferences.advancedDescription "Configure how Tor Browser connects to the internet">
<!ENTITY torPreferences.advancedButton "Settings…">
<!ENTITY torPreferences.viewTorLogs "View the Tor logs">
<!ENTITY torPreferences.viewLogs "ログを表示…">
<!-- Remove all bridges dialog -->
<!ENTITY torPreferences.removeBridgesQuestion "Remove all the bridges?">
<!ENTITY torPreferences.removeBridgesWarning "この操作は元に戻すことはできません。">
<!ENTITY torPreferences.cancel "キャンセル">
<!-- Scan bridge QR dialog -->
<!ENTITY torPreferences.scanQrTitle "Scan the QR code">
<!-- Builtin bridges dialog -->
<!ENTITY torPreferences.builtinBridgeTitle "Built-In Bridges">
<!ENTITY torPreferences.builtinBridgeDescription "Tor Browser includes some specific types of bridges known as “pluggable transports”.">
<!ENTITY torPreferences.builtinBridgeObfs4 "obfs4">
<!ENTITY torPreferences.builtinBridgeObfs4Description "obfs4 is a type of built-in bridge that makes your Tor traffic look random. They are also less likely to be blocked than their predecessors, obfs3 bridges.">
<!ENTITY torPreferences.builtinBridgeSnowflake "Snowflake">
<!ENTITY torPreferences.builtinBridgeSnowflakeDescription "Snowflake is a built-in bridge that defeats censorship by routing your connection through Snowflake proxies, ran by volunteers.">
<!ENTITY torPreferences.builtinBridgeMeekAzure "meek-azure">
<!ENTITY torPreferences.builtinBridgeMeekAzureDescription "meek-azure is a built-in bridge that makes it look like you are using a Microsoft web site instead of using Tor.">
<!-- Request bridges dialog -->
<!ENTITY torPreferences.requestBridgeDialogTitle "Bridge をリクエスト">
<!ENTITY torPreferences.requestBridgeDialogWaitPrompt "BridgeDB に接続しています。しばらくお待ちください。">
<!ENTITY torPreferences.requestBridgeDialogSolvePrompt "Bridge をリクエストするには CAPTCHA を解いて下さい。">
<!ENTITY torPreferences.requestBridgeErrorBadSolution "その解答は正しくありません。再度実行してください。">
<!-- Provide bridge dialog -->
<!ENTITY torPreferences.provideBridgeTitle "Provide Bridge">
<!ENTITY torPreferences.provideBridgeHeader "信頼できる情報源から入手したブリッジの情報を入力してください。">
<!-- Connection settings dialog -->
<!ENTITY torPreferences.connectionSettingsDialogTitle "Connection Settings">
<!ENTITY torPreferences.connectionSettingsDialogHeader "Configure how Tor Browser connects to the Internet">
<!ENTITY torPreferences.firewallPortsPlaceholder "Comma-seperated values">
<!-- Log dialog -->
<!ENTITY torPreferences.torLogsDialogTitle "Tor ログ">

<!-- #24746 about:torconnect strings -->
<!ENTITY torConnect.notConnectedConcise "接続していません">
<!ENTITY torConnect.connectingConcise "接続中…">
<!ENTITY torConnect.tryingAgain "Trying again…">
<!ENTITY torConnect.noInternet "Tor Browser couldn’t reach the Internet">
<!ENTITY torConnect.couldNotConnect "Tor Browser could not connect to Tor">
<!ENTITY torConnect.assistDescriptionConfigure "configure your connection"> <!-- used as a text to insert as a link on several strings (#1) -->
<!ENTITY torConnect.assistDescription "If Tor is blocked in your location, trying a bridge may help. Connection assist can choose one for you using your location, or you can #1 manually instead."> <!-- #1 = "configure your connection" link -->
<!ENTITY torConnect.tryingBridge "Trying a bridge…">
<!ENTITY torConnect.tryingBridgeAgain "Trying one more time…">
<!ENTITY torConnect.errorLocation "Tor Browser couldn’t locate you">
<!ENTITY torConnect.errorLocationDescription "Tor Browser needs to know your location in order to choose the right bridge for you. If you’d rather not share your location, #1 manually instead."> <!-- #1 = "configure your connection" link -->
<!ENTITY torConnect.isLocationCorrect "Are these location settings correct?">
<!ENTITY torConnect.isLocationCorrectDescription "Tor Browser still couldn’t connect to Tor. Please check your location settings are correct and try again, or #1 instead."> <!-- #1 = "configure your connection" link -->
<!ENTITY torConnect.breadcrumbAssist "Connection assist">
<!ENTITY torConnect.breadcrumbLocation "Location settings">
<!ENTITY torConnect.breadcrumbTryBridge "Try a bridge">
<!ENTITY torConnect.automatic "自動">
<!ENTITY torConnect.selectCountryRegion "Select Country or Region">
<!ENTITY torConnect.frequentLocations "Frequently selected locations">
<!ENTITY torConnect.otherLocations "Other locations">
<!ENTITY torConnect.restartTorBrowser "Restart Tor Browser">
<!ENTITY torConnect.configureConnection "接続を構成…">
<!ENTITY torConnect.viewLog "View logs…">
<!ENTITY torConnect.tryAgain "もう一度やり直してください">
<!ENTITY torConnect.offline "Internet not reachable">
<!ENTITY torConnect.connectMessage "Tor 設定の変更は接続するまで反映されません">
<!ENTITY torConnect.tryAgainMessage "Tor Browser は、Tor ネットワークへの接続を確立できませんでした">
<!ENTITY torConnect.yourLocation "Your Location">
<!ENTITY torConnect.tryBridge "ブリッジを試す">
<!ENTITY torConnect.autoBootstrappingFailed "Automatic configuration failed">
<!ENTITY torConnect.autoBootstrappingFailed "Automatic configuration failed">
<!ENTITY torConnect.cannotDetermineCountry "Unable to determine user country">
<!ENTITY torConnect.noSettingsForCountry "No settings available for your location">
