<!ENTITY torsettings.dialog.title "Tor-ქსელის პარამეტრები">
<!ENTITY torsettings.wizard.title.default "Tor-თან დაკავშირება">
<!ENTITY torsettings.wizard.title.configure "Tor-ის ქსელის პარამეტრები">
<!ENTITY torsettings.wizard.title.connecting "კავშირის დამყარება">

<!-- For locale picker: -->
<!ENTITY torlauncher.localePicker.title "Tor-ბრაუზერის ენა">
<!ENTITY torlauncher.localePicker.prompt "გთხოვთ, აირჩიოთ ენა.">

<!-- For "first run" wizard: -->

<!ENTITY torSettings.connectPrompt "Tor-თან დასაკავშირებლად დააწკაპეთ „დაკავშირებაზე“.">
<!ENTITY torSettings.configurePrompt "დააწკაპეთ „გამართვაზე“ ქსელის სათანადო პარამეტრების შესარჩევად, თუ ისეთ ქვეყანაში იმყოფებით, სადაც შეზღუდულია Tor (ასეთებია ეგვიპტე, ჩინეთი, თურქეთი) ან თუ ისეთი კერძო ქსელიდან ცდილობთ დაკავშირებას, რომელიც საჭიროებს პროქსის.">
<!ENTITY torSettings.configure "გამართვა">
<!ENTITY torSettings.connect "დაკავშირება">

<!-- Other: -->

<!ENTITY torsettings.startingTor "ელოდება Tor-ს გასაშვებად...">
<!ENTITY torsettings.restartTor "Tor-ის ხელახლა გაშვება">
<!ENTITY torsettings.reconfigTor "ხელახლა გამართვა">

<!ENTITY torsettings.discardSettings.prompt "თქვენ უკვე გამართული გაქვთ Tor-ის გადამცემი ხიდები ან მითითებული გაქვთ ადგილობრივი პროქსის პარამეტრები.&#160; Tor-ქსელთან პირდაპირ დასაკავშირებლად, საჭიროა ამ პარამეტრების მოცილება.">
<!ENTITY torsettings.discardSettings.proceed "პარამეტრების მოცილება და დაკავშირება">

<!ENTITY torsettings.optional "არასავალდებულო">

<!ENTITY torsettings.useProxy.checkbox "ინტერნეტთან დასაკავშირებლად პროქსი გამოიყენება">
<!ENTITY torsettings.useProxy.type "პროქსის სახეობა">
<!ENTITY torsettings.useProxy.type.placeholder "მიუთითეთ პროქსის სახეობა">
<!ENTITY torsettings.useProxy.address "მისამართი">
<!ENTITY torsettings.useProxy.address.placeholder "IP-მისამართი ან დასახელება">
<!ENTITY torsettings.useProxy.port "პორტი">
<!ENTITY torsettings.useProxy.username "მეტსახელი">
<!ENTITY torsettings.useProxy.password "პაროლი">
<!ENTITY torsettings.useProxy.type.socks4 "SOCKS 4">
<!ENTITY torsettings.useProxy.type.socks5 "SOCKS 5">
<!ENTITY torsettings.useProxy.type.http "HTTP / HTTPS">
<!ENTITY torsettings.firewall.checkbox "კომპიუტერი ინტერნეტში გადის ქსელის ფარის გავლით, მხოლოდ გარკვეული პორტებით">
<!ENTITY torsettings.firewall.allowedPorts "დაშვებული პორტები">
<!ENTITY torsettings.useBridges.checkbox "Tor შეზღუდულია ჩემს ქვეყანაში">
<!ENTITY torsettings.useBridges.default "ჩაშენებული გადამცემი ხიდის მითითება">
<!ENTITY torsettings.useBridges.default.placeholder "ხიდის მითითება">
<!ENTITY torsettings.useBridges.bridgeDB "გადამცემი ხიდის მოთხოვნა საიტიდან torproject.org">
<!ENTITY torsettings.useBridges.captchaSolution.placeholder "შეიყვანეთ სურათიდან სიმბოლოები">
<!ENTITY torsettings.useBridges.reloadCaptcha.tooltip "ახალი კოდის მიღება">
<!ENTITY torsettings.useBridges.captchaSubmit "გაგზავნა">
<!ENTITY torsettings.useBridges.custom "ნაცნობი ხიდის მითითება">
<!ENTITY torsettings.useBridges.label "შეიყვანეთ გადამცემი ხიდის მონაცემები სანდო წყაროდან.">
<!ENTITY torsettings.useBridges.placeholder "აკრიფეთ მისამართი:პორტი (თითო ცალკე ხაზზე)">

<!ENTITY torsettings.copyLog "Tor-ის აღრიცხვის ჩანაწერების ასლი">

<!ENTITY torsettings.proxyHelpTitle "პროქსის შესახებ">
<!ENTITY torsettings.proxyHelp1 "ადგილობრივი პროქსი მაშინაა საჭირო, როცა ინტერნეტს უკავშირდებით დაწესებულების, სკოლის ან უმაღლესი სასწავლებლის ქსელის გავლით.&#160;თუ დარწმუნებული არ ხართ პროქსის საჭიროებაში, გადახედეთ ინტერნეტის პარამეტრებს სხვა ბრაუზერში ან იხილეთ სისტემის ქსელის პარამეტრები.">

<!ENTITY torsettings.bridgeHelpTitle "დახმარება გადამცემი ხიდის თაობაზე">
<!ENTITY torsettings.bridgeHelp1 "ხიდები წარმოადგენს აღუნუსხავ გადამცემებს, რომელთა გამოყენებაც ართულებს Tor-ქსელთან კავშირის შეზღუდვას.&#160; თითოეული სახის ხიდი იყენებს განსხვავებულ საშუალებებს, ცენზურის ასარიდებლად.&#160; obfs-გადამყვანი, გადაცემულ მონაცემებს წარმოადგენს შემთხვევითი ხმაურის სახით, ხოლო meek-გადამყვანი კი თქვენს მონაცემთა მიმოცვლას წარმოაჩენს ისე, თითქოს ცალკეულ მომსახურებას უკავშირდებით და არა Tor-ს.">
<!ENTITY torsettings.bridgeHelp2 "იმისდა მიხედვით, თუ როგორ ზღუდავს ცალკეული ქვეყანა Tor-ს, გარკვეული სახის გადამცემი ხიდები, მხოლოდ შესაბამის ქვეყნებში იმუშავებს და არა სხვაგან.&#160; თუ დანამდვილებით არ იცით, რომელი გადამცემი მუშაობს თქვენს ქვეყანაში, ეწვიეთ დახმარების გვერდს torproject.org/about/contact.html#support">

<!-- Progress -->
<!ENTITY torprogress.pleaseWait "გთხოვთ, მოითმინოთ სანამ დაუკავშირდება Tor-ქსელს.&#160; რამდენიმე წუთს შეიძლება გასტანოს.">

<!-- #31286 about:preferences strings -->
<!ENTITY torPreferences.categoryTitle "დაკავშირება">
<!ENTITY torPreferences.torSettings "Tor-ის პარამეტრები">
<!ENTITY torPreferences.torSettingsDescription "Tor-ბრაუზერი თქვენს კავშირს ატარებს Tor-ქსელის გავლით, რომლის მუშაობასაც უზრუნველყოფს ათასობით მოხალისე მთელი მსოფლიოდან." >
<!ENTITY torPreferences.learnMore "იხილეთ ვრცლად">
<!-- Status -->
<!ENTITY torPreferences.statusInternetLabel "ინტერნეტი:">
<!ENTITY torPreferences.statusInternetTest "ტესტი">
<!ENTITY torPreferences.statusInternetOnline "ხაზზეა">
<!ENTITY torPreferences.statusInternetOffline "კავშირგარეშე">
<!ENTITY torPreferences.statusTorLabel "Tor-ქსელი:">
<!ENTITY torPreferences.statusTorConnected "მიერთებული">
<!ENTITY torPreferences.statusTorNotConnected "არაა დაკავშირებული">
<!ENTITY torPreferences.statusTorBlocked "სავარაუდოდ იზღუდება">
<!ENTITY torPreferences.learnMore "იხილეთ ვრცლად">
<!-- Quickstart -->
<!ENTITY torPreferences.quickstart "სწაფგაშვება">
<!ENTITY torPreferences.quickstartDescriptionLong "სწრაფგაშვებით Tor-ბრაუზერი თავადვე დაუკავშირდება Tor-ქსელს გაშვებისას, ბოლოს გამოყენებული პარამეტრებით.">
<!ENTITY torPreferences.quickstartCheckbox "ყოველთვის თვითდაკავშირება">
<!-- Bridge settings -->
<!ENTITY torPreferences.bridges "გადამცემი ხიდები">
<!ENTITY torPreferences.bridgesDescription "გადამცემი ხიდების მეშვეობით შეგიძლიათ დაუკავშირდეთ Tor-ქსელს იქ, სადაც Tor-ს ზღუდავენ. იმისდა მიხედვით, თუ სად იმყოფებით, ზოგმა ხიდმა უკეთ შეიძლება იმუშაოს, ვიდრე სხვამ.">
<!ENTITY torPreferences.bridgeLocation "თქვენი მდებარეობა">
<!ENTITY torPreferences.bridgeLocationAutomatic "თვითშერჩევა">
<!ENTITY torPreferences.bridgeLocationFrequent "ხშირად მითითებული მდებარეობა">
<!ENTITY torPreferences.bridgeLocationOther "სხვა მდებარეობა">
<!ENTITY torPreferences.bridgeChooseForMe "ხიდის შერჩევა...">
<!ENTITY torPreferences.bridgeBadgeCurrent "თქვენი მიმდინარე ხიდები">
<!ENTITY torPreferences.bridgeBadgeCurrentDescription "შეგიძლიათ დაამახსოვროთ ერთი ან რამდენიმე ხიდი და Tor თავადვე აირჩევს დაკავშირებისას. საჭიროების შემთხვევაშიც, Tor თავადვე შეძლებს სხვა ხიდზე გადართვას.">
<!ENTITY torPreferences.bridgeId "#1 ხიდი: #2"> <!-- #1 = bridge type; #2 = bridge emoji id -->
<!ENTITY torPreferences.remove "წაშლა">
<!ENTITY torPreferences.bridgeDisableBuiltIn "ჩაშენებული ხიდების გათიშვა">
<!ENTITY torPreferences.bridgeShare "გააზიარეთ ხიდი QR-კოდით ან მისი მისამართის ასლით.">
<!ENTITY torPreferences.bridgeCopy "ხიდის მისამართის ასლი">
<!ENTITY torPreferences.copied "ასლი აღებულია!">
<!ENTITY torPreferences.bridgeShowAll "ყველა ხიდის გამოჩენა">
<!ENTITY torPreferences.bridgeRemoveAll "ყველა ხიდის მოცილება">
<!ENTITY torPreferences.bridgeAdd "ახალი ხიდის დამატება">
<!ENTITY torPreferences.bridgeSelectBrowserBuiltin "ამორჩევა Tor-ბრაუზერის ჩაშენებული ხიდებიდან">
<!ENTITY torPreferences.bridgeSelectBuiltin "ჩაშენებული ხიდის მითითება...">
<!ENTITY torPreferences.bridgeRequest "გადამცემი ხიდის მოთხოვნა...">
<!ENTITY torPreferences.bridgeEnterKnown "უკვე ნაცნობი ხიდის შეყვანა">
<!ENTITY torPreferences.bridgeAddManually "ხიდის ხელით მითითება...">
<!-- Advanced settings -->
<!ENTITY torPreferences.advanced "დამატებით">
<!ENTITY torPreferences.advancedDescription "მიუთითეთ, თუ როგორ დაუკავშირდეს Tor-ბრაუზერი ინტერნეტს">
<!ENTITY torPreferences.advancedButton "პარამეტრები...">
<!ENTITY torPreferences.viewTorLogs "იხილეთ Tor-ის ჩანაწერები">
<!ENTITY torPreferences.viewLogs "ჩანაწერების ნახვა...">
<!-- Remove all bridges dialog -->
<!ENTITY torPreferences.removeBridgesQuestion "მოცილდეს ყველა ხიდი?">
<!ENTITY torPreferences.removeBridgesWarning "ეს ქმედება შეუქცევადია.">
<!ENTITY torPreferences.cancel "გაუქმება">
<!-- Scan bridge QR dialog -->
<!ENTITY torPreferences.scanQrTitle "QR-კოდის წაკითხვა">
<!-- Builtin bridges dialog -->
<!ENTITY torPreferences.builtinBridgeTitle "ჩაშენებული ხიდები">
<!ENTITY torPreferences.builtinBridgeDescription "Tor-ბრაუზერი შეიცავს გარკვეული სახის ხიდებს, ე.წ. „მისაერთებელ გადამყვანებს“.">
<!ENTITY torPreferences.builtinBridgeObfs4 "obfs4">
<!ENTITY torPreferences.builtinBridgeObfs4Description "obfs4 სახეობის ჩაშენებული ხიდი Tor-ში გადაცემულ მონაცემებს წარმოადგენს შემთხვევითი ხმაურის სახით. უფრო ნაკლებია მისი შეზღუდვის ალბათობა, ვიდრე წინა თაობის obfs3-ხიდებისა.">
<!ENTITY torPreferences.builtinBridgeSnowflake "Snowflake">
<!ENTITY torPreferences.builtinBridgeSnowflakeDescription "Snowflake ჩაშენებული ხიდია, რომელიც გვერდს უვლის ცენზურას კავშირების გატარებით მოხალისეების მიერ გამართული Snowflake-პროქსის მეშვეობით.">
<!ENTITY torPreferences.builtinBridgeMeekAzure "meek-azure">
<!ENTITY torPreferences.builtinBridgeMeekAzureDescription "meek-azure სახეობის ჩაშენებული ხიდი ისე წარმოაჩენს, თითქოს Microsoft-ის საიტს უკავშირდებოდეთ, ნაცვლად Tor-ისა.">
<!-- Request bridges dialog -->
<!ENTITY torPreferences.requestBridgeDialogTitle "ხიდის მოთხოვნა">
<!ENTITY torPreferences.requestBridgeDialogWaitPrompt "უკავშირდება BridgeDB-ს. გთხოვთ, მოითმინოთ.">
<!ENTITY torPreferences.requestBridgeDialogSolvePrompt "გაიარეთ CAPTCHA, გადამცემი ხიდის მოთხოვნისთვის.">
<!ENTITY torPreferences.requestBridgeErrorBadSolution "გადაწყვეტა არასწორია. გთხოვთ სცადოთ ხელახლა.">
<!-- Provide bridge dialog -->
<!ENTITY torPreferences.provideBridgeTitle "ხიდის მიწოდება">
<!ENTITY torPreferences.provideBridgeHeader "შეიყვანეთ ხიდის მონაცემები სანდო წყაროდან">
<!-- Connection settings dialog -->
<!ENTITY torPreferences.connectionSettingsDialogTitle "კავშირის პარამეტრები">
<!ENTITY torPreferences.connectionSettingsDialogHeader "მიუთითეთ, თუ როგორ დაუკავშირდეს Tor-ბრაუზერი ინტერნეტს">
<!ENTITY torPreferences.firewallPortsPlaceholder "მძიმით გამოყოფილი მნიშვნელობები">
<!-- Log dialog -->
<!ENTITY torPreferences.torLogsDialogTitle "Tor-ის ჩანაწერები">

<!-- #24746 about:torconnect strings -->
<!ENTITY torConnect.notConnectedConcise "არაა დაკავშირებული">
<!ENTITY torConnect.connectingConcise "უკავშირდება...">
<!ENTITY torConnect.tryingAgain "ცდილობს ხელახლა...">
<!ENTITY torConnect.noInternet "Tor-ბრაუზერი ვერ უკავშირდება ქსელს">
<!ENTITY torConnect.couldNotConnect "Tor-ბრაუზერი ვერ უკავშირდება Tor-ს">
<!ENTITY torConnect.assistDescriptionConfigure "კავშირის გამართვა"> <!-- used as a text to insert as a link on several strings (#1) -->
<!ENTITY torConnect.assistDescription "თუ Tor იზღუდება თქვენს ადგილსამყოფელზე, ხიდი გამოგადგებათ. კავშირის მეგზური აგირჩევთ რომელიმეს მდებარეობის მიხედვით, ანდაც შეგიძლიათ #1 ხელით მიუთითოთ"> <!-- #1 = "configure your connection" link -->
<!ENTITY torConnect.tryingBridge "ხიდის მოსინჯვა...">
<!ENTITY torConnect.tryingBridgeAgain "კიდევ ერთი მცდელობა...">
<!ENTITY torConnect.errorLocation "Tor-ბრაუზერი ვერ ადგენს მდებარეობას">
<!ENTITY torConnect.errorLocationDescription "Tor-ბრაუზერი საჭიროებს თქვენ მდებარეობას, რომ შესაფერისი ხიდი შეგირჩიოთ. თუ არ გსურთ გამჟღავნება, #1 მიუთითეთ ხელით."> <!-- #1 = "configure your connection" link -->
<!ENTITY torConnect.isLocationCorrect "მდებარეობის პარამეტრები სწორია?">
<!ENTITY torConnect.isLocationCorrectDescription "Tor-ბრაუზერი კვლავ ვერ უკავშირდება Tor-ქსელს. გთხოვთ, შეამოწმოთ თქვენი მდებარეობის სისწორე და სცადოთ ხელახლა ან #1."> <!-- #1 = "configure your connection" link -->
<!ENTITY torConnect.breadcrumbAssist "კავშირის მეგზური">
<!ENTITY torConnect.breadcrumbLocation "მდებარეობის პარამეტრები">
<!ENTITY torConnect.breadcrumbTryBridge "ხიდის მოსინჯვა">
<!ENTITY torConnect.automatic "თვითშერჩევა">
<!ENTITY torConnect.selectCountryRegion "აირჩიეთ ქვეყანა ან მხარე">
<!ENTITY torConnect.frequentLocations "ხშირად მითითებული მდებარეობა">
<!ENTITY torConnect.otherLocations "სხვა მდებარეობა">
<!ENTITY torConnect.restartTorBrowser "Tor-ბრაუზერის ხელახლა გაშვება">
<!ENTITY torConnect.configureConnection "კავშირის გამართვა...">
<!ENTITY torConnect.viewLog "ჩანაწერების ნახვა...">
<!ENTITY torConnect.tryAgain "სცადეთ ხელახლა">
<!ENTITY torConnect.offline "ქსელი მიუწვდომელია">
<!ENTITY torConnect.connectMessage "ცვლილებები Tor-ის პარამეტრებში არ აისახება, სანამ არ დაუკავშირდებით">
<!ENTITY torConnect.tryAgainMessage "Tor-ბრაუზერმა ვერ მოახერხა კავშირის დამყარება Tor-ქსელთან">
<!ENTITY torConnect.yourLocation "თქვენი მდებარეობა">
<!ENTITY torConnect.tryBridge "მოსინჯეთ ხიდი">
<!ENTITY torConnect.autoBootstrappingFailed "თვითგამართვა ვერ მოხერხდა">
<!ENTITY torConnect.autoBootstrappingFailed "თვითგამართვა ვერ მოხერხდა">
<!ENTITY torConnect.cannotDetermineCountry "ვერ დგინდება მომხმარებლის ქვეყანა">
<!ENTITY torConnect.noSettingsForCountry "პარამეტრები არაა თქვენი მდებარეობისთვის">
